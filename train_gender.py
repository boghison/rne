from model import load_model
import keras
from helper import Train
from keras.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from keras.callbacks import ReduceLROnPlateau

import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="3"

patience = 50
epochs = 100

log_file_path = "genderlog.txt"
trained_models_path = 'models/gender'

model = load_model('gender')
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
model.summary()

early_stop = EarlyStopping('val_loss', patience=patience)
reduce_lr = ReduceLROnPlateau('val_loss', factor=0.1,
                              patience=int(patience/2), verbose=1)
csv_logger = CSVLogger(log_file_path, append=False)
model_names = trained_models_path + '.{epoch:02d}-{val_acc:.2f}.hdf5'
model_checkpoint = ModelCheckpoint(model_names,
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=True,
                                   save_weights_only=False)
callbacks = [model_checkpoint, csv_logger, early_stop, reduce_lr]

img_hndl = Train('gender')

model.fit_generator(img_hndl.flow(),
                    steps_per_epoch=img_hndl.STEPS_TRAIN,
                    epochs = epochs,
                    verbose =1,
                    callbacks = callbacks,
                    validation_data = img_hndl.flow('valid'),
                    validation_steps=img_hndl.STEPS_VALID)
