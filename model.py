import keras
from keras.layers import Input, TimeDistributed, Lambda, Flatten, Dropout, Activation, LSTM, Dense, BatchNormalization
from keras.models import Model
from keras.models import load_model as load

from keras.regularizers import l2

#import constant
from helper import LSTM_WINDOW


def load_model(mode):
	if mode == 'emotion':
		model = load('emoriginal.hdf5')
		inputs = Input(shape = (LSTM_WINDOW, 64, 64, 1))
		classes = 7
	else:
		model = load('genoriginal.hdf5')
		inputs = Input(shape = (LSTM_WINDOW, 48, 48, 3))
		classes = 2

	# as per https://github.com/keras-team/keras/issues/8772
	model.summary()
	new_model = Model(model.inputs, model.layers[-3].output)
	new_model.summary()
	new_model.set_weights(model.get_weights())

	x = TimeDistributed(Lambda(lambda x: x/127.5 - 1.0))(inputs)
	x = TimeDistributed(new_model)(x)
	x = TimeDistributed(Flatten())(x)

	x = LSTM(128, return_sequences = True, stateful = False)(x)
	x = Dropout(0.5)(x)
	x = Activation('tanh')(x)
	x = LSTM(128, return_sequences = False, stateful = False)(x)
	x = Dropout(0.5)(x)
	x = Activation('tanh')(x)

	x = Dense(128, kernel_regularizer=l2(0.001))(x)
	x = BatchNormalization()(x)
	x = Dropout(0.5)(x)

	output1 = Dense(classes, activation='softmax', name='predictions_1')(x)
	return Model(inputs, output1)
