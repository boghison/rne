import cv2
from pathlib import Path
from inference import *
import random
from random import shuffle
import re
from numpy import asarray

#some constants
DATASET_PATH_GEN = '../dataset_lstm/gender'
DATASET_PATH_EM = '../dataset_lstm/emotion'
LSTM_WINDOW = 8
BATCH_SIZE = 1
#from original code
gender_offsets = (30, 60)
emotion_offsets = (20, 40)
gender_target_size = (48, 48)
emotion_target_size = (64, 64)
face_detection = load_detection_model('face.xml')

# emotion and gender classification is in the file path
match = re.compile('(\d\d)-(\d\d)-(\d\d)-(\d\d)-(\d\d)-(\d\d)-(\d\d)')


def process_path(path):
    groups = match.search(str(path)).groups()[1:]

    return groups

def emotion(group):
    #adapt ravdess to their classification
    emotion = [0, 0, 0, 0, 0, 0, 0]
    ravdess = int(group[1])
    if ravdess == 2: ravdess = 1
    if ravdess == 1: emotion[6] = 1
    if ravdess == 3: emotion[3] = 1
    if ravdess == 4: emotion[4] = 1
    if ravdess == 5: emotion[0] = 1
    if ravdess == 6: emotion[2] = 1
    if ravdess == 7: emotion[1] = 1
    if ravdess == 8: emotion[5] = 1

    return emotion

def gender(group):
    gender = [0, 0] #male, female
    actor = int(group[5])
    # even numbered actors are female
    if actor % 2 == 0:
        gender[0] = 1
    else:
        gender[1] = 1

    return gender


#this function splits into chunks and clones/removes paths to make them fit in LSTM_WINDOW
def normalize(paths):
    imgs = paths
    window = LSTM_WINDOW
    rest = len(imgs) % window
    if rest <= window/2 and rest != 0:
        for i in range(rest): imgs.pop(random.randint(0,len(imgs)-1))
    elif rest > window/2:
        for i in range(window-rest):
            pos = random.randint(0, len(imgs) - 1)
            imgs.insert(pos, imgs[pos])
    return list(chunks(imgs, window))

def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

class Train:
    def __init__(self, mode='emotion'):
        if mode == 'emotion':
            self.y_func = emotion
            self.DATASET_PATH = DATASET_PATH_EM
        if mode == 'gender':
            self.y_func = gender
            self.DATASET_PATH = DATASET_PATH_GEN
        self.mode = mode
        self.split_dataset()
        self.STEPS_TRAIN = int(len(self.train_set_paths)/BATCH_SIZE)
        self.STEPS_VALID = int(len(self.validation_set_paths)/BATCH_SIZE)

    def split_dataset(self):
        self.train_set_paths = []
        self.validation_set_paths = []
        paths = []
        for path in Path(self.DATASET_PATH).iterdir():
            #make sure that paths are taken sequentially
            files = list(sorted(path.iterdir()))
            files =normalize(files)
            which = random.random()
            # split into training and validation dataset
            if which < 0.7:
                # so now it is a list of lists of size lstm_window
                self.train_set_paths.extend(files)
            else:
                self.validation_set_paths.extend(files)
        shuffle(self.train_set_paths)
        shuffle(self.validation_set_paths)


    def process(self, paths):
        imgs = []
        for path in paths:
            if self.mode == 'emotion':
                img = cv2.imread(str(path), 0)
                gray_face = preprocess_input(img, False)
                gray_face = np.expand_dims(gray_face, -1)
                imgs.append(gray_face)
            if self.mode == 'gender':
                img = cv2.imread(str(path))
                rgb_face = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                rgb_face = preprocess_input(rgb_face, False)
                imgs.append(rgb_face)
        return asarray(imgs)

    def flow(self, mode = 'train'):
        flow_paths = []
        if mode == 'train':
            flow_paths = self.train_set_paths
        if mode == 'valid':
            flow_paths = self.validation_set_paths

        input = []
        targets = []
        while True:
            for lstm_chunk in flow_paths:
                # lstm_chunk is a chunk of size lstm_window that contains some paths
                y = self.y_func(process_path(lstm_chunk[0]))
                x = self.process(lstm_chunk)
                input.append(x)
                targets.append(y)
                if len(input) == BATCH_SIZE:
                    yield(asarray(input), asarray(targets))
                    input = []
                    targets = []
            shuffle(flow_paths)
